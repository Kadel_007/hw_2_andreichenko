# Напишите программу которая выполнит следующее:
#
# Попросить ввести свой возраст (можно исползовать константу или input()).
#
# - если пользователю меньше 7 - вывести “Где твои мама и папа?”
#
# - если пользователю меньше 18 - вывести “Мы не продаем сигареты несовершеннолетним!”
#
# - если пользователю больше 65 - вывести “Вы в зоне риска”
#
# - если у пользователя юбилей (10, 20, 40 и тд лет) - вывести “Где сертификат от вакцинации?”
#
# - в любом другом случае - вывести “Оденьте маску! ”


def main():
    age = 1
    while age != 0:
        print("Please enter your integer age. (Hint: enter 0 to exit program)")
        try:
            age = int(input())
        except:
            print("It is not integer number!")
            return
        res_txt = ""
        if age != 0 and age < 7:
            res_txt = "Where are your mother and father?\n"

        elif age != 0 and age % 10 == 0 and age < 18:
            res_txt = "We don't sell cigarettes to children. And where is vaccination certificate?\n"

        elif age != 0 and age < 18:
            res_txt = "We don't sell cigarettes to children.\n"

        elif age > 65 and age % 10 == 0:
            res_txt = "You are in dangerous! Where is vaccination certificate?\n"

        elif age != 0 and age % 10 == 0:
            res_txt = "Where is vaccination certificate?\n"

        elif age > 65:
            res_txt = "You are in dangerous!\n"

        else:
            if age != 0:
                res_txt = "Please put on your mask.\n"
            else:
                res_txt = "Thank you!\n"
        print(res_txt)
    
main()